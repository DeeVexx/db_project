import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Model.RecordModel;
import services.Database;
import services.Table;
import services.Util;
public class MainInterface extends javax.swing.JFrame{
	
	private javax.swing.JButton cmdCommit;
    private javax.swing.JButton cmdExecute;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtResult;
    private javax.swing.JTextField txtSQL;
    
    private String currentDatabase = "";  
    int idx = 0; 

	Database db = new Database();	
	Table tbl = new Table();
	RecordModel isCreated = null;
	RecordModel isInserted = null;
	RecordModel tableValues = null;
	List<RecordModel> updatedTableData = new ArrayList<>();
	List<RecordModel> deletedTableData = new ArrayList<>();

	int tempSize = 0;

	RecordModel record = new RecordModel();
	
    
    public MainInterface() {
		createApp();
	}
    
    public void createApp() {
		txtSQL = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtResult = new javax.swing.JTextArea();
        cmdExecute = new javax.swing.JButton("Execute");
        cmdCommit = new javax.swing.JButton("Commit");
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Final Assignment");

        txtSQL.setFont(new java.awt.Font("Lucida Console", 0, 12)); // NOI18N
        txtSQL.setText("");
        txtResult.setEditable(false);
        txtResult.setColumns(20);
        txtResult.setFont(new java.awt.Font("Lucida Console", 0, 13)); // NOI18N
        txtResult.setForeground(new java.awt.Color(0, 0, 102));
        txtResult.setRows(5);
        txtResult.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtResult.setDisabledTextColor(new java.awt.Color(0, 0, 102));
        jScrollPane1.setViewportView(txtResult);
        
        cmdExecute.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
        		cmdExecuteActionPerformed(evt);
        	}
        });
        
        cmdCommit.addActionListener(new java.awt.event.ActionListener() {
        	public void actionPerformed(java.awt.event.ActionEvent evt) {
        		cmdCommitActionPerformed(evt);
        	}
        });
        
        cmdCommit.setEnabled(false);
    	
		
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addGroup(layout.createSequentialGroup()
	                        .addGap(28, 28, 28)
	                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
	                            .addComponent(jLabel1)
	                            .addComponent(jLabel2))
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
	                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 751, Short.MAX_VALUE)
	                            .addComponent(txtSQL)))
	                    .addGroup(layout.createSequentialGroup()
	                        .addGap(334, 334, 334)
	                        .addComponent(cmdExecute)
	                        .addGap(130, 130, 130)
	                        .addComponent(cmdCommit))
	                    .addGroup(layout.createSequentialGroup()
	                        .addGap(355, 355, 355)
	                        .addComponent(jLabel3)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 397, javax.swing.GroupLayout.PREFERRED_SIZE)))
	                .addContainerGap(93, Short.MAX_VALUE))
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addGap(26, 26, 26)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(jLabel3)
	                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(txtSQL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(jLabel1))
	                .addGap(18, 18, 18)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addComponent(jLabel2)
	                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addGap(18, 18, 18)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(cmdCommit)
	                    .addComponent(cmdExecute))
	                .addContainerGap(58, Short.MAX_VALUE))
	        );
	        
	        pack();
	}

    private void cmdExecuteActionPerformed(java.awt.event.ActionEvent evt) {
		txtResult.setText("");
		boolean isValid = ProcessSyntax(txtSQL.getText(), false);	
		
    }
	
    private void cmdCommitActionPerformed(java.awt.event.ActionEvent evt) {
    	txtResult.setText("");
		boolean isValid = ProcessSyntax(txtSQL.getText(), true);	
		if(isValid) {
			cmdCommit.setEnabled(false);
		}
    }
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
				}
			}
		}catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MainInterface().setVisible(true);
			}
		});
	
	}
	
	
	private boolean ProcessSyntax(String userSql, boolean comm){    	
    	boolean isValid = false;
    	
    	if(userSql.isBlank()) {
    		txtResult.setText("Enter a SQL command");
    		return isValid;
    	}
    	
    	final String create = "^(?i)CREATE +(DATABASE +[A-Za-z]+[0-9A-Za-z]*|TABLE +[A-Za-z]+[0-9A-Za-z]* +[(]+[ ]*+[A-Za-z]+[0-9A-Za-z]*+[ ]+(INT|STRING)+([,]+[ ]*+[A-Za-z]+[0-9A-Za-z]*+[ ]+(INT|STRING))*+[ ]*+[)]|INDEX+[ ]+ON+[ ]+[A-Za-z]+[0-9A-Za-z]*+[(]+[A-Za-z]+[0-9A-Za-z]*+[ ]*+[,]*+[ ]*+[A-Za-z]*+[0-9A-Za-z]*+[)])+[ ]*+[;]$";
    	final String use = "^(?i)USE +[A-Za-z]+[0-9A-Za-z]*+[ ]*+[;]$";
    	final String select = "^(?i)SELECT +([A-Za-z]+[0-9A-Za-z]*|[*])+[ ]+FROM+[ ]+[A-Za-z]+[0-9A-Za-z]*+( WHERE+[ ]+[A-Za-z]+[0-9A-Za-z]*+[ ]*+(<|>|=|!=|<=|>=)+[ ]*+[']*+([A-Za-z]+[0-9A-Za-z]|[0-9])*+[']*)*+[ ]*+[;]$";
    	final String insert = "^(?i)INSERT INTO +[A-Za-z]+[0-9A-Za-z]*+ VALUES+[ ]*+[(]+[ ]*+(\\d+|[']+\\S+['])+([ ]*+[,]+[ ]*+(\\d+|[']+\\S+[']))*+[)]+[ ]*+[;]$";
    	final String delete ="^(?i)DELETE FROM +[A-Za-z]+[0-9A-Za-z]*+[ ]*+(where +[A-Za-z]+[0-9A-Za-z]*+[ ]*+(<|>|=|!=|<=|>=)+[ ]*+(\\d+|[']+\\S+[']))*+[ ]*+[;]$";
    	final String update = "^(?i)UPDATE +[A-Za-z]+[0-9A-Za-z]*+( SET+[ ]+[A-Za-z]+[0-9A-Za-z]*+[ ]*+(=)+[ ]*+[']*+[0-9A-Za-z]*)*+[']*+( WHERE+[ ]+[A-Za-z]+[0-9A-Za-z]*+[ ]*+(<|>|=|!=|<=|>=)+[ ]*+[']*+[0-9A-Za-z]*+[']*)+[ ]*+[;]$";
        final String show = "^(?i)SHOW +(databases|tables)+[ ]*+[;]$";
    	
    	String[] words = userSql.trim().split("\\s+");
		String type = words[1].replace(";", "");
    	
    	if(words.length < 2) {
    		return isValid;
    	}
    	String sqlCmd = userSql.substring(0, userSql.indexOf(' '));    	
    	
    	switch(sqlCmd.toUpperCase()) {
    	    case "CREATE":
    	    	if( Pattern.matches(create, userSql)) {
    	    		String dbName = words[2].replace(";", "");
    	    		
    	    		if(type.equalsIgnoreCase("DATABASE")) {    	    			
        				if(db.CreateDatabase(dbName)) {
        					txtResult.setText("Database " + dbName + " created");
        				}else {
        					txtResult.setText("Error creating database " + dbName );
        				}
    	    		}else if(type.equalsIgnoreCase("TABLE")){
    	    			String tabName = words[2].trim();
    	    			
    	    			if(currentDatabase.isBlank()) {
    	    				txtResult.setText("No database selected");
    	    				break;
    	    			}
    	    			
    	    			if(isCreated == null) {
    	    				isCreated = tbl.CreateTable(userSql);
        	    			
        	    			if(isCreated!=null) {
        	    				txtResult.setText(String.format("Table %s created.", tabName));
        	    				cmdCommit.setEnabled(true);
        	    				
        	    			}else {
        	    				
        	    				txtResult.setText("ERR::Fail to create table");
        	    			}
    	    			}
    	    			
    	    			if(isCreated!= null && comm) {
    	    				if(tbl.CommitTable(currentDatabase, tabName, isCreated)) {
    	    					txtResult.setText(String.format("%s committed to disk", tabName));
    	    				}else {
    	    					txtResult.setText(String.format("Fail to commit %s to disk", tabName));
    	    				}
    	    			}
    	    			
    	    			
    	    		}else if(type.equalsIgnoreCase("INDEX")){
    	    			
    	    		}
    	    		
    	    		isValid = true;
    	    	}else {
    	    		txtResult.setText("ERROR INVALID SQL");
    	    	}
    	    	
    	    	break;
    	    	
    	    case "USE":
    	    	if(Pattern.matches(use, userSql)){
    	    		String path = "./data/"+ words[1].replace(";", "");
    	    		
    	    		if(Util.ValidateDatabase(path)) {
    	    			currentDatabase = words[1].replace(";", "");
	    				jLabel5.setText(currentDatabase.toUpperCase());
	    				txtResult.setText("Switched to db " + currentDatabase);
	    				isValid = true;
    	    		}else {
	    				txtResult.setText("Database does not exist");
    				}
    	    	}else {
    	    		txtResult.setText("ERROR INVALID SQL");
    	    	}
    	    	
    	    	break;
    	    
    	    case "SHOW":
    	    	
    	    		if(Pattern.matches(show, userSql)) {
    				
    				if(currentDatabase.isEmpty() && (type.equalsIgnoreCase("TABLES"))) {
        				txtResult.setText("No database selected.");
        				break;
        			}
	    				List<String> results = new ArrayList<String>();
	    				
	    				results = db.Show(type, currentDatabase);
	    				
	    				if(results.size() < 1) {
	    					txtResult.setText("No result.");
	    				}else {
	    					txtResult.setText(String.format("%d table(s) in %s.\n", results.size(), currentDatabase));
	    					txtResult.setText(txtResult.getText() + "_____________________________________________________\n\n");
	    					for(String file : results){
	    						txtResult.setText(txtResult.getText() + file.replace(".data", "") +"\n");
	    					}
	    				}
	    				
	    				isValid = true;
	    			}else {
	    	    		txtResult.setText("ERROR INVALID SQL");
	    	    	}
    	    	
    	    	break;
    	    	
    	    case "INSERT":
    	    	
    	    	if(Pattern.matches(insert, userSql)) {
    	    		String tabName = words[2].trim();
	    			
	    			if(currentDatabase.isBlank()) {
	    				txtResult.setText("No database selected");
	    				break;
	    			}
	    			   
	    			String fullPath = String.format("./data/%s/%s.data", currentDatabase, tabName);
	    			if(!Util.ValidateTable(fullPath)) {
	    				txtResult.setText("Operation fail. Table does not exist."); 
	    				break;
	    			}
    	    		
    	    		if(tableValues == null) {
    	    			
    	    			tableValues = Util.getSqlValues(userSql, 1);
    	    			
    	    			record.setColumn1(tableValues.getColumn1());
    	    			record.setColumn2(tableValues.getColumn2());
    	    			record.setColumnId(tableValues.getColumnId());
    	    			
    	    			txtResult.setText("Operation fail"); 
    	    			 
    	    			if(record != null) {
    	    				txtResult.setText(String.format("Record(s) inserted into table %s.", tabName));
    	    				cmdCommit.setEnabled(true);
    	    			}    	    				
    	    			
    	    			break;
    	    		}  	  
    	    		
    	    		if(record != null && comm) {
    	    			
    	    			if(tbl.insert(currentDatabase, tabName, record, false)) {
	    					txtResult.setText(String.format("Record(s) committed to disk %s.", tabName));
	    					tableValues = null;
    	    			}else {
	    					txtResult.setText(String.format("Fail to commit records to disk %s.", tabName));
	    				}
    	    		}
    	    		
    	    		isValid = true;
    	    		
    	    	}else {
    	    		txtResult.setText("ERROR INVALID SQL");
    	    	}
    	    		
    	    	break;
    	    	
    	    case "SELECT":
    	    	if(Pattern.matches(select, userSql)) {
    	    		
    	    		String tabName = words[3].replace(";", "").trim();
	    			
	    			if(currentDatabase.isBlank()) {
	    				txtResult.setText("No database selected");
	    				break;
	    			}
    	    		
	    			String fullPath = String.format("./data/%s/%s.data", currentDatabase, tabName);
	    			if(!Util.ValidateTable(fullPath)) {
	    				txtResult.setText("Operation fail. Table does not exist."); 
	    				break;
	    			}
	    			
	    			List<RecordModel> tableData = tbl.ReadData(currentDatabase, tabName);
	    			RecordModel tableHeader = tableData.get(0);
    				txtResult.setText(String.format("%s\t|\t%s\t|\t%s", tableHeader.getColumnId(), tableHeader.getColumn1(), tableHeader.getColumn2()));
    				txtResult.setText(txtResult.getText() + "\n_________________________________________________________________________________________");
	    			
	    			if(!userSql.toUpperCase().contains("WHERE")) {	    				
	    				
	    				for(int i = 1; i < tableData.size(); i++) {
	    					RecordModel result = tableData.get(i);
	    					txtResult.setText(txtResult.getText() + "\n" + result.getColumnId() + "\t|\t" + result.getColumn1() + "\t|\t" + result.getColumn2());
	    					
	    				}
	    				
	    			}else {
	    				
	    				Matcher columns = Pattern.compile("\\bWHERE\\s+(.*?)$").matcher(userSql.toUpperCase());
	    				String col = "";
	    				String[] columnsArr;
	    				
	    				while(columns.find()) {
	    					col = columns.group(1).replace(";", "");
	    				}
	    				
	    				columnsArr = col.trim().split(" ");
	    				String searchCol = columnsArr[0];
	    				String condition = columnsArr[1];
	    				String searchValue = columnsArr[2];
	    				searchValue = searchValue.replace("'", "");
	    				
	    				switch(condition) {
		    				case "=":

		    					for(int i = 1; i < tableData.size(); i++) {
		    						RecordModel result = tableData.get(i);
		    						
		    						if(searchCol.equalsIgnoreCase(tableHeader.getColumnId())){
		    							if(Integer.parseInt(tableData.get(i).getColumnId()) == Integer.parseInt(searchValue)) {
		    								txtResult.setText(txtResult.getText() + "\n" + result.getColumnId() + "\t|\t" + result.getColumn1() + "\t|\t" + result.getColumn2() );
		    								continue;
		    							}
		    						}else if(searchCol.equalsIgnoreCase(tableHeader.getColumn1())) {
		    							if(tableData.get(i).getColumn1().equalsIgnoreCase(searchValue)) {
		    								txtResult.setText(txtResult.getText() + "\n" + result.getColumnId() + "\t|\t" + result.getColumn1() + "\t|\t" + result.getColumn2() );
		    								continue;
		    							}
		    						}else if(searchCol.equalsIgnoreCase(tableHeader.getColumn2())) {
		    							if(tableData.get(i).getColumn2().equalsIgnoreCase(searchValue)) {
		    								txtResult.setText(txtResult.getText() + "\n" + result.getColumnId() + "\t|\t" + result.getColumn1() + "\t|\t" + result.getColumn2() );
		    								continue;
		    							}
		    						}		    						
		    						
		    					}
		    					
		    					break;
		    				case "!=":
		    					for(int i = 1; i < tableData.size(); i++) {
		    						RecordModel result = tableData.get(i);
		    						
		    						if(searchCol.equalsIgnoreCase(tableHeader.getColumnId())){
		    							if(Integer.parseInt(tableData.get(i).getColumnId()) != Integer.parseInt(searchValue)) {
		    								txtResult.setText(txtResult.getText() + "\n" + result.getColumnId() + "\t|\t" + result.getColumn1() + "\t|\t" + result.getColumn2() );
		    								continue;
		    							}
		    						}else if(searchCol.equalsIgnoreCase(tableHeader.getColumn1())) {
		    							if(!tableData.get(i).getColumn1().equalsIgnoreCase(searchValue)) {
		    								txtResult.setText(txtResult.getText() + "\n" + result.getColumnId() + "\t|\t" + result.getColumn1() + "\t|\t" + result.getColumn2() );
		    								continue;
		    							}
		    						}else if(searchCol.equalsIgnoreCase(tableHeader.getColumn2())) {
		    							if(!tableData.get(i).getColumn2().equalsIgnoreCase(searchValue)) {
		    								txtResult.setText(txtResult.getText() + "\n" + result.getColumnId() + "\t|\t" + result.getColumn1() + "\t|\t" + result.getColumn2() );
		    								continue;
		    							}
		    						}		    						
		    						
		    					}
		    					
		    					break;
		    				case ">":
		    					
		    					for(int i = 1; i < tableData.size(); i++) {
		    						RecordModel result = tableData.get(i);
		    						
		    						if(searchCol.equalsIgnoreCase(tableHeader.getColumnId())){
		    							if(Integer.parseInt(tableData.get(i).getColumnId()) > Integer.parseInt(searchValue)) {
		    								txtResult.setText(txtResult.getText() + "\n" + result.getColumnId() + "\t|\t" + result.getColumn1() + "\t|\t" + result.getColumn2() );
		    							}
		    						}    						
		    						
		    					}
		    					
		    					break;
		    				case "<":
		    					
		    					for(int i = 1; i < tableData.size(); i++) {
		    						RecordModel result = tableData.get(i);
		    						
		    						if(searchCol.equalsIgnoreCase(tableHeader.getColumnId())){
		    							if(Integer.parseInt(tableData.get(i).getColumnId()) < Integer.parseInt(searchValue)) {
		    								txtResult.setText(txtResult.getText() + "\n" + result.getColumnId() + "\t|\t" + result.getColumn1() + "\t|\t" + result.getColumn2() );
		    							}
		    						}    						
		    						
		    					}
		    					
		    					break;
		    				default:
		    					txtResult.setText("Invalid operation");
		    					break;
	    				}
	    			}
	    			
	    			isValid = true;
    	    		
    	    	}else {
    	    		txtResult.setText("ERROR INVALID SQL");
    	    	}
    	    	
    	    	break;
    	    	
    	    case "UPDATE":
    	    	
    	    	if(Pattern.matches(update, userSql)) {
    	    		String tabName = words[1].trim();
    	    		
    	    		if(currentDatabase.isBlank()) {
	    				txtResult.setText("No database selected");
	    				break;
	    			}
    	    		
	    			String fullPath = String.format("./data/%s/%s.data", currentDatabase, tabName);
	    			if(!Util.ValidateTable(fullPath)) {
	    				txtResult.setText("Operation fail. Table does not exist."); 
	    				break;
	    			}
	    			    				
    				
    				if(!comm) {
    					updatedTableData = tbl.ReadData(currentDatabase, tabName);
    	    			RecordModel tableHeader = updatedTableData.get(0);
    	    			Matcher whereCondition = Pattern.compile("\\bWHERE\\s+(.*?)$").matcher(userSql.toUpperCase());
        				String col = "";
        				String[] conditions;
        				
        				while(whereCondition.find()) {
        					col = whereCondition.group(1).replace(";", "");
        				}
        				
        				conditions = col.trim().split(" ");
        				
        				String searchCol = conditions[0];
        				String condition = conditions[1];
        				String searchValue = conditions[2].replace("'", "");
        				String replacementValue = words[5].trim().replace("'", "");
        				String replacementcolumn = words[3].trim();
        				
	    				switch(condition) {
		    				case "=":
		    					for(int i = 1; i < updatedTableData.size(); i++) {
		        					
		        					if(searchCol.equalsIgnoreCase(tableHeader.getColumnId())){
		    							if(Integer.parseInt(updatedTableData.get(i).getColumnId()) == Integer.parseInt(searchValue)) {
		    								if(tableHeader.getColumnId().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumnId(replacementValue);continue;
		    								}
		    								else if(tableHeader.getColumn1().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumn1(replacementValue);continue;
		    								}
		    								else if(tableHeader.getColumn2().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumn2(replacementValue);continue;
		    								}
		    							}
		    						}  
		        					else if(searchCol.equalsIgnoreCase(tableHeader.getColumn1())){
		    							if(updatedTableData.get(i).getColumn1() == searchValue) {
		    								if(tableHeader.getColumnId().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumnId(replacementValue);continue;
		    								}
		    								else if(tableHeader.getColumn1().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumn1(replacementValue);continue;
		    								}
		    								else if(tableHeader.getColumn2().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumn2(replacementValue);continue;
		    								}
		    							}
		    						}   
		        					else if(searchCol.equalsIgnoreCase(tableHeader.getColumn2())){
		    							if(updatedTableData.get(i).getColumn2() == searchValue) {
		    								if(tableHeader.getColumnId().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumnId(replacementValue);continue;
		    								}
		    								else if(tableHeader.getColumn1().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumn1(replacementValue);continue;
		    								}
		    								else if(tableHeader.getColumn2().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumn2(replacementValue);continue;
		    								}
		    							}
		    						}   
		        					
		        				}  
			    					break;
			    					
		    				case ">":
		    					for(int i = 1; i < updatedTableData.size(); i++) {
		        					
		        					if(searchCol.equalsIgnoreCase(tableHeader.getColumnId())){
		    							if(Integer.parseInt(updatedTableData.get(i).getColumnId()) > Integer.parseInt(searchValue)) {
		    								if(tableHeader.getColumnId().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumnId(replacementValue);continue;
		    								}
		    								else if(tableHeader.getColumn1().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumn1(replacementValue);continue;
		    								}
		    								else if(tableHeader.getColumn2().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumn2(replacementValue);continue;
		    								}
		    							}
		    						} 
		        					
		        				}  
		    					break;
		    					
		    				case "<":
		    					for(int i = 1; i < updatedTableData.size(); i++) {
		        					
		        					if(searchCol.equalsIgnoreCase(tableHeader.getColumnId())){
		    							if(Integer.parseInt(updatedTableData.get(i).getColumnId()) < Integer.parseInt(searchValue)) {
		    								if(tableHeader.getColumnId().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumnId(replacementValue);continue;
		    								}
		    								else if(tableHeader.getColumn1().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumn1(replacementValue);continue;
		    								}
		    								else if(tableHeader.getColumn2().equalsIgnoreCase(replacementcolumn)){
		    									updatedTableData.get(i).setColumn2(replacementValue);continue;
		    								}
		    							}
		    						} 
		        					
		        				}  
		    					break;
		    					
		    				default:
		    					txtResult.setText("Operation fail");
		    					break;
	    				}
    				cmdCommit.setEnabled(true);
    	    	}
    				if(updatedTableData != null && comm) {
						
						try {
							String tablePath = String.format("%s/%s/%s.data", "./data", currentDatabase, tabName);
    						Path path = Paths.get(tablePath);
							Files.write(path, new byte[0], StandardOpenOption.TRUNCATE_EXISTING);
							
							
							for(int i = 0; i < updatedTableData.size(); i++) {
    							if(i == 0 ) {
    								tbl.UpdateTable(currentDatabase, tabName, updatedTableData.get(0), true);
    							}else {
    								tbl.UpdateTable(currentDatabase, tabName, updatedTableData.get(i), false);
    							}
    						}
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
					}
    				
    				isValid = true;
	    			
    	    	}else {
    	    		txtResult.setText("ERROR INVALID SQL");
    	    	}
    	    	
    	    	break;
    	    
    	    case "DELETE":
    	    	
    	    	if(Pattern.matches(delete, userSql)) {
    	    		String tabName = words[2].replace(";", "").trim();
	    			if(currentDatabase.isBlank()) {
	    				txtResult.setText("No database selected");
	    				break;
	    			}
    	    		
	    			String fullPath = String.format("./data/%s/%s.data", currentDatabase, tabName);
	    			if(!Util.ValidateTable(fullPath)) {
	    				txtResult.setText("Operation fail. Table does not exist."); 
	    				break;
	    			}
	    			
	    			if(!comm) {
	    				
	    				deletedTableData = tbl.ReadData(currentDatabase, tabName);
	    				
	    				tempSize = deletedTableData.size();
	    				RecordModel tableHeader = deletedTableData.get(0);
	    				
		    			if(!userSql.toUpperCase().contains("WHERE")) {	    				
		    				
		    				txtResult.setText(String.format("%d rows deleted", deletedTableData.size() - 1));
		    				
		    			}else {
		    				
		    				Matcher columns = Pattern.compile("\\bWHERE\\s+(.*?)$").matcher(userSql.toUpperCase());
		    				String col = "";
		    				String[] columnsArr;
		    				
		    				while(columns.find()) {
		    					col = columns.group(1).replace(";", "");
		    				}
		    				
		    				columnsArr = col.trim().split(" ");
		    				String searchCol = columnsArr[0];
		    				String condition = columnsArr[1];
		    				String searchValue = columnsArr[2];
		    				searchValue = searchValue.replace("'", "");
		    				
		    				switch(condition) {
		    					case "=":
		    						for(int i = 1; i < deletedTableData.size(); i++) {
			        					
			        					if(searchCol.equalsIgnoreCase(tableHeader.getColumnId())){
			    							if(Integer.parseInt(deletedTableData.get(i).getColumnId()) == Integer.parseInt(searchValue)) {			    								
			    									deletedTableData.remove(i); continue;
			    								}
			    							
			    						} else if(searchCol.equalsIgnoreCase(tableHeader.getColumn1())){
			    							if(deletedTableData.get(i).getColumn1() == searchValue) {
			    								
			    									deletedTableData.remove(i); continue;
			    							}
			    						}   
			        					else if(searchCol.equalsIgnoreCase(tableHeader.getColumn2())){
			    							if(deletedTableData.get(i).getColumn2() == searchValue) {
			    									deletedTableData.remove(i); continue;
			    							}
			    						}   
		    						}
		    						
		    						break;
		    						
		    					case ">":
			    					for(int i = 1; i < deletedTableData.size(); i++) {
			        					
			        					if(searchCol.equalsIgnoreCase(tableHeader.getColumnId())){
			    							if(Integer.parseInt(deletedTableData.get(i).getColumnId()) > Integer.parseInt(searchValue)) {
			    								
			    									deletedTableData.remove(i); continue;
			    							}
			    						} 
			        					
			        				}  
			    					break;
			    					
		    					case "<":
			    					for(int i = 1; i < deletedTableData.size(); i++) {
			        					
			        					if(searchCol.equalsIgnoreCase(tableHeader.getColumnId())){
			    							if(Integer.parseInt(deletedTableData.get(i).getColumnId()) < Integer.parseInt(searchValue)) {
			    								
			    									deletedTableData.remove(i); continue;
			    							}
			    						} 
			        					
			        				}  
			    					break;
		    						
		    					default:	    						
		    						txtResult.setText("Operation fail");
		    						break;
		    				}
		    			}
		    			
	    			}
	    			
	    			if(deletedTableData != null && comm) {	 
	    					
							try {
									String tablePath = String.format("%s/%s/%s.data", "./data", currentDatabase, tabName);
		    						Path path = Paths.get(tablePath);
									Files.write(path, new byte[0], StandardOpenOption.TRUNCATE_EXISTING);
									
									
									
									if(!userSql.toUpperCase().contains("WHERE")) {	    
										tbl.UpdateTable(currentDatabase, tabName, deletedTableData.get(0), true);
									}else {
										
										for(int i = 0; i < deletedTableData.size(); i++) {
			    							if(i == 0 ) {
			    								tbl.UpdateTable(currentDatabase, tabName, deletedTableData.get(0), true);
			    							}else {
			    								 tbl.UpdateTable(currentDatabase, tabName, deletedTableData.get(i), false);
			    							}
			    						}
									}									
									 
								
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
	    			}
	    			if(tempSize > deletedTableData.size()) {
	    				txtResult.setText(String.format("%d rows successfully deleted.", tempSize - deletedTableData.size()));
	    			}
	    			isValid = true;
	    			cmdCommit.setEnabled(true);
    	    	}else {
    	    		txtResult.setText("ERROR INVALID SQL");
    	    	}
    	    	
    	    	break;
    	    	
    		default:
    			txtResult.setText("Invalid Command");
    			break;
    	}
    	
    	
    	return isValid;
	}
	
}
