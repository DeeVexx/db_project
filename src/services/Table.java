package services;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Model.RecordModel;

public class Table implements Serializable {
	
	private static final long serialVersionUID = 6529685098267757690L;
	
	private final String basePath = "./data";	
	
	public RecordModel CreateTable(String sql) {
		
		RecordModel tableHeader = new RecordModel();		
		
		try {			
			
				tableHeader = Util.getSqlValues(sql, 0);
			
		}catch(Exception e) {
			System.out.println("Table creation failed");
			tableHeader = null;
		}
		
		return tableHeader;
	}

	public boolean CommitTable(String db, String tblName, RecordModel tbl) {
		boolean committed = false;
		
		try(ByteArrayOutputStream bos = new ByteArrayOutputStream();
				ObjectOutputStream out = new ObjectOutputStream(bos);) {
			
			out.writeObject(tbl);
			out.flush();
			
			byte[] tableByte = bos.toByteArray();
			
			String tablePath = String.format("%s/%s/%s.data", basePath, db, tblName);
			
			File table = new File(tablePath);
			table.createNewFile();
			
			FileOutputStream outFile = new FileOutputStream(table, true);
			outFile.write(tableByte);
			outFile.close();
			
			committed = true;
			
			
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		return committed;
	}
	
	public boolean insert(String db, String tblName, RecordModel data, boolean isIndexed) {
		boolean committed = false;
			
		if(isIndexed) {
			
		}else {
			
			try(ByteArrayOutputStream bos = new ByteArrayOutputStream();
					ObjectOutputStream out = new ObjectOutputStream(bos);) {				
				
				out.writeObject(data);
				out.flush();
				
				byte[] tableByte = bos.toByteArray();
				
				String tablePath = String.format("%s/%s/%s.data", basePath, db, tblName);
				
				FileOutputStream outFile = new FileOutputStream(tablePath, true);

				outFile.write("\n".getBytes());
				outFile.write(tableByte);
				outFile.close();
				
				committed = true;
				
				
			}catch(IOException e) {
				e.printStackTrace();
			}
			
		}
		
		return committed;
	}
	
	public List<RecordModel> ReadData(String db, String tblName) {
		List<RecordModel> records = new ArrayList<>();
		String tablePath = String.format("%s/%s/%s.data", basePath, db, tblName);		

	    	ObjectInput in = null;
			try(BufferedReader br = new BufferedReader(new FileReader(tablePath))) {
				
			    for(String line; (line = br.readLine()) != null; ) {
			        // process the line.
			    	System.out.println(line);			    	
			    	
			    	ByteArrayInputStream bis = new ByteArrayInputStream(line.getBytes());
			    	in = new ObjectInputStream(bis);
			    	RecordModel record = (RecordModel) in.readObject();
			    	records.add(record);
			    }
			    
			    
			    // line is not visible here.
			} 
			catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
		
		return records;
		
	}

	public boolean UpdateTable(String db, String tblName, RecordModel newValue, boolean header) {
		boolean isUpdated = false;
		
		String tablePath = String.format("%s/%s/%s.data", basePath, db, tblName);
		try(ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);
				ObjectOutputStream out = new ObjectOutputStream(bos);) {			
			
			out.writeObject(newValue);
			out.flush();
			
			byte[] tableByte = bos.toByteArray();
			
			FileOutputStream outFile = new FileOutputStream(tablePath, true);

			if(!header) {
				outFile.write("\n".getBytes());
			}
			
			outFile.write(tableByte);
			outFile.close();
			
			isUpdated = true;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return isUpdated;
	}	
	
}
