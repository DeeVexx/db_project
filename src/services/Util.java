package services;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Model.RecordModel;


public class Util {
	
	public static RecordModel getSqlValues(String sql, int op) {
		
		Matcher columns = Pattern.compile("\\((.*?)\\)").matcher(sql);
		RecordModel tableValues = new RecordModel();
		String col = "";
		String[] columnsArr; 
		
		while(columns.find()) {
			col = columns.group(1);
		}
	
		columnsArr = col.trim().split(",");
		
		if(op == 0) {
			columnsArr[0] = columnsArr[0].trim();
			columnsArr[1] = columnsArr[1].trim();
			columnsArr[2] = columnsArr[2].trim();

			tableValues.setColumnId(columnsArr[0].substring(0, columnsArr[0].indexOf(' ')));
			tableValues.setColumn1(columnsArr[1].substring(0, columnsArr[1].indexOf(' ')));
			tableValues.setColumn2(columnsArr[2].substring(0, columnsArr[2].indexOf(' ')));
		}else {
			tableValues.setColumnId(columnsArr[0].trim());
			tableValues.setColumn1(columnsArr[1].trim().replace("'", ""));
			tableValues.setColumn2(columnsArr[2].trim().replace("'", ""));
		}
		
		System.out.print(tableValues);
		
		return tableValues;
	}

	public static boolean ValidateDatabase(String db) {
		if(Files.isDirectory(Paths.get(db))) {
			return true;
		}else {
			return false;
		}
	}
	
	public static boolean ValidateTable(String table) {
		Path path = Paths.get(table);
		if(Files.isRegularFile(path)) {
			return true;
		}else {
			return false;
		}
	}
}
