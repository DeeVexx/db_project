package services;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Database {

	 
    private final String basePath = "./data/";
    
	public boolean CreateDatabase(String dbName) {
		
		File db = new File(basePath + dbName);
		return db.mkdirs();
	}	

	public List<String> Show(String type, String db){
		List<String> results = new ArrayList<String>();
		String fullPath = String.format("%s%s", basePath, db);
		
		if(type.equalsIgnoreCase("TABLES")) {
			File[] files = new File(fullPath).listFiles();

			for (File file : files) {
			    if (file.isFile()) {
			        results.add(file.getName());
			    }
			}
		}else {
			File directoryPath = new File(basePath);
			String dbs[] = directoryPath.list();
			
			for(String database : dbs) {
				results.add(database);
			}
		}
		
		return results;
	}
}
