package Model;

import java.io.Serializable;

public class RecordModel implements Serializable {
	
	private static final long serialVersionUID = 6529685098267757690L;
	
	private String column1;
	private String column2;
	private String columnId;
	
	
	public String getColumn1() {
		return column1;
	}
	public void setColumn1(String column1) {
		this.column1 = column1;
	}
	public String getColumn2() {
		return column2;
	}
	public void setColumn2(String column2) {
		this.column2 = column2;
	}
	public String getColumnId() {
		return columnId;
	}
	public void setColumnId(String columnId) {
		this.columnId = columnId;
	}
	
	
}
